# CE Europa Bot

## A Telegram bot dedicated to CE Europa

A Telegram bot dedicated to CE Europa (Spanish 3rd Division football club). Returns league stats and match and player information.
Add on Telegram by searching: CE Europa Bot.