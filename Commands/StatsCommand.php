<?php
namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;

class StatsCommand extends UserCommand
{
	
	protected $name = 'stats';
    protected $description = 'Returns current statistics (league position, wins, losses, draws)';
    protected $usage = '/stats';
    protected $version = '1.0.0';

    public function execute()
    {

        $string = file_get_contents("./Data/stats.json");
        $json_a = json_decode($string, true);

        foreach($json_a as $key => $value) {
            $position = $value['position'];
            $points = $value['points_total'];
            $played_total = $value['played_total'];
            $won_total = $value['wins_total'];
            $drawn_total = $value['draws_total'];
            $lost_total = $value['losses_total'];
            $goals_scored_total = $value['goals_scored_total'];
            $clean_sheets_total = $value['clean_sheets_total'];
        }

        $text = 'Posició Actual/Current League Position: ' . $position . PHP_EOL;
        $text .= 'Punts/Points: ' . $points . PHP_EOL;
        $text .= 'Partits Jugats/Played: ' . $played_total . PHP_EOL;
        $text .= 'Partits Guanyats/Won: ' . $won_total . PHP_EOL;
        $text .= 'Partits EmpatatsDrawn: ' . $drawn_total . PHP_EOL;
        $text .= 'Partits Perduts/Lost: ' . $lost_total . PHP_EOL;
        $text .= 'Gols/Goals Scored: ' . $goals_scored_total . PHP_EOL;
        $text .= 'Partits Sense Encaixar/Clean Sheets: ' . $clean_sheets_total . PHP_EOL;

        $message = $this->getMessage();

        $chat_id = $message->getChat()->getId();

        $data = [
            'chat_id' => $chat_id,
            'text'    => $text,
        ];

        return Request::sendMessage($data);
    }
}