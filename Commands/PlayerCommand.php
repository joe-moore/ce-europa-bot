<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;

/**
 * Start command
 */
class PlayerCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'player';

    /**
     * @var string
     */
    protected $description = 'Player command';

    /**
     * @var string
     */
    protected $usage = '/player';

    /**
     * @var string
     */
    protected $version = '1.0.0';

    /**
     * Command execute method
     *
     * @return mixed
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {

        $string = file_get_contents("./Data/players.json");
        $json_a = json_decode($string, true);

        $message = $this->getMessage();
        $messageText = $message->getText();

        $messageText = str_replace( "/player ", "", $messageText);

        $normalizeChars = array(
            'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
            'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
            'Ï'=>'I', 'Ñ'=>'N', 'Ń'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
            'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
            'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
            'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ń'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
            'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f',
            'ă'=>'a', 'î'=>'i', 'â'=>'a', 'ș'=>'s', 'ț'=>'t', 'Ă'=>'A', 'Î'=>'I', 'Â'=>'A', 'Ș'=>'S', 'Ț'=>'T',
        );

        $messageText = strtr( $messageText, $normalizeChars );

        $stringToSearch = strtolower( $messageText );

        $response = array();

        foreach ( $json_a as $key => $value ) {
            if ( strtolower( $value['name'] ) == $stringToSearch ) {
                $response[] = $value['name'] . ": " . $value['position'] . "\n";
                $response[] .= "Número/Number: " . $value['number'] . "\n";
                $response[] .= "Procedència/Previous Club: " . $value['previous_club'] . "\n";
                $response[] .= "Nom Complet/Full Name: " . $value['fullname'] . "\n";
                $response[] .= "Data de naixement/DOB: " . $value['dob'] . "\n";
                $photo = $value['photo'];
            }
        }

    	$chat_id = $message->getChat()->getId();

        if ( is_array( $response ) && !empty( $response ) ) {
            $text = implode( "", $response );
            $result = Request::sendPhoto([
			    'chat_id' => $chat_id,
			    'photo'   => Request::encodeFile( "https://dev.j-moore.uk/CEEuropaBot/Data/" . $photo ),
			    'caption' => $text
			]);
        } else {
            $text = "Ho sento. No he pogut trobar aquest jugador.\nSorry, I couldn't find that player.";
            $data = [                                  // Set up the new message data
	            'chat_id' => $chat_id,                 // Set Chat ID to send the message to
	            'text'    => $text, // Set message to send
	        ];
        }       

        /*function writeDebug( $data ){

            $logfile = "debugLog.log";
            file_put_contents($logfile, date("Y-m-d H:i:s").$data."</br>", FILE_APPEND);

        }

        writeDebug( $text );*/

        //return Request::sendMessage($data);        // Send message!
    }
}
