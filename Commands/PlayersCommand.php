<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;

/**
 * Start command
 */
class PlayersCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'players';

    /**
     * @var string
     */
    protected $description = 'Players command';

    /**
     * @var string
     */
    protected $usage = '/players';

    /**
     * @var string
     */
    protected $version = '1.0.0';

    /**
     * Command execute method
     *
     * @return mixed
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {

        $string = file_get_contents("./Data/players.json");
        $json_a = json_decode($string, true);

        $message = $this->getMessage();
        $messageText = $message->getText();

        $messageText = str_replace( "/players ", "", $messageText);

        $messageText = iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $messageText);

        $stringToSearch = strtolower( $messageText );

        $response = array();

        foreach ( $json_a as $key => $value ) {
            if ( strtolower( $value['position_gen'] ) == $stringToSearch ) {
                $response[] = $value['name'] . " - " . $value['position'] . "\n";
            }
        }

        if ( is_array( $response ) && !empty( $response ) ) {
            $text = implode( "", $response);
        } else {
            $text = "No m'has enviat una posició vàlida.\nThat is not a valid position.";
        }

        $chat_id = $message->getChat()->getId();

        $data = [                                  // Set up the new message data
            'chat_id' => $chat_id,                 // Set Chat ID to send the message to
            'text'    => $text, // Set message to send
        ];

        /*function writeDebug( $data ){

            $logfile = "debugLog.log";
            file_put_contents($logfile, date("Y-m-d H:i:s").$data."</br>", FILE_APPEND);

        }

        writeDebug( $text );*/

        return Request::sendMessage($data);        // Send message!
    }
}
