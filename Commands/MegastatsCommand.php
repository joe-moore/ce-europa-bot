<?php
namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;

class MegastatsCommand extends UserCommand
{
	
	protected $name = 'stats';
    protected $description = 'Returns current statistics (league position, wins, losses, draws)';
    protected $usage = '/stats';
    protected $version = '1.0.0';

    public function execute()
    {

        $string = file_get_contents("./Data/stats.json");
        $json_a = json_decode($string, true);

        foreach($json_a as $key => $value) {
            $position = $value['position'];
            $points = $value['points_total'];
            $played_total = $value['played_total'];
            $played_home = $value['played_home'];
            $played_away = $value['played_away'];
            $won_total = $value['wins_total'];
            $won_home = $value['wins_home'];
            $won_away = $value['wins_away'];
            $drawn_total = $value['draws_total'];
            $drawn_home = $value['draws_home'];
            $drawn_away = $value['draws_away'];
            $lost_total = $value['losses_total'];
            $lost_home = $value['losses_home'];
            $lost_away = $value['losses_away'];
            $goals_scored_total = $value['goals_scored_total'];
            $goals_scored_home = $value['goals_scored_home'];
            $goals_scored_away = $value['goals_scored_away'];
            $goals_against_total = $value['goals_against_total'];
            $goals_against_home = $value['goals_against_home'];
            $goals_against_away = $value['goals_against_away'];
            $clean_sheets_total = $value['clean_sheets_total'];
            $clean_sheets_home = $value['clean_sheets_home'];
            $clean_sheets_away = $value['clean_sheets_away'];
        }

        $text = 'Posició Actual/Current League Position: ' . $position . PHP_EOL;
        $text .= 'Punts/Points: ' . $points . PHP_EOL;
        $text .= 'Partits Jugats/Played: ' . $played_total . ' (A Casa/Home: ' . $played_home . ', Fora/Away: ' . $played_away . ')' . PHP_EOL;
        $text .= 'Partits Guanyats/Won: ' . $won_total . ' (A Casa/Home: ' . $won_home . ', Fora/Away: ' . $won_away . ')' . PHP_EOL;
        $text .= 'Partits Empatats/Drawn: ' . $drawn_total . ' (A Casa/Home: ' . $drawn_home . ', Fora/Away: ' . $drawn_away . ')' . PHP_EOL;
        $text .= 'Partits Perduts/Lost: ' . $lost_total . ' (A Casa/Home: ' . $lost_home . ', Fora/Away: ' . $lost_away . ')' . PHP_EOL;
        $text .= 'Gols/Goals Scored: ' . $goals_scored_total . ' (A Casa/Home: ' . $goals_scored_home . ', Fora/Away: ' . $goals_scored_away . ')' . PHP_EOL;
        $text .= 'Gols en contra/Goals against: ' . $goals_against_total . ' (A Casa/Home: ' . $goals_against_home . ', Fora/Away: ' . $goals_against_away . ')' . PHP_EOL;
        $text .= 'Partits Sense Encaixar/Clean Sheets: ' . $clean_sheets_total . ' (A Casa/Home: ' . $clean_sheets_home . ', Fora/Away: ' . $clean_sheets_away . ')' . PHP_EOL;

        $message = $this->getMessage();

        $chat_id = $message->getChat()->getId();

        $data = [
            'chat_id' => $chat_id,
            'text'    => $text,
        ];

        return Request::sendMessage($data);
    }
}