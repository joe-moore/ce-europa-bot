<?php
namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;

class NextmatchCommand extends UserCommand
{
	
	protected $name = 'nextmatch';
    protected $description = 'Shows next match';
    protected $usage = '/nextmatch';
    protected $version = '1.0.0';

    public function execute()
    {
    	$string = file_get_contents("./Data/fixtures.json");
        $json_a = json_decode($string, true);

        foreach($json_a as $key => $value) {
            $next_match = $value['next_match'];
        }

        $text = $next_match;

        $message = $this->getMessage();

        $chat_id = $message->getChat()->getId();

        $data = [
            'chat_id' => $chat_id,
            'text'    => "El següent partit ès: " . $text . "\nNext match is: " . $text,
        ];

        return Request::sendMessage($data);
    }
}