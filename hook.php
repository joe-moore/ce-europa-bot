<?php
// Load composer
require __DIR__ . '/vendor/autoload.php';

$bot_api_key  = '670812176:AAFijUdP-3OD6348XiFDt1IiPb4rELst9Rk';
$bot_username = 'CEEuropaBot';

$commands_paths = [
    __DIR__ . '/Commands',
];

try {
    // Create Telegram API object
    $telegram = new Longman\TelegramBot\Telegram($bot_api_key, $bot_username);
    $telegram->addCommandsPaths($commands_paths);

    // Handle telegram webhook request
    $telegram->handle();

} catch (Longman\TelegramBot\Exception\TelegramException $e) {
    // Silence is golden!
    // log telegram errors
    // echo $e->getMessage();
}